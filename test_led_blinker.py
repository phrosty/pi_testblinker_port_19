import RPi.GPIO as GPIO
import time

# LED Pin
LED_PIN = 19

# Blink interval
blink_interval = .5 # Time interval in seconds

# Configure the PIN #8
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)

# Blinker loop
try:
    while True:
        GPIO.output(LED_PIN, True)
        time.sleep(blink_interval)
        GPIO.output(LED_PIN, False)
        time.sleep(blink_interval)

except KeyboardInterrupt():
    GPIO.cleanup()

except Exception as e:
    GPIO.cleanup()
    print(e)

finally:
    GPIO.cleanup()
    print("Good bye!")


# pi_testblinker_port_19

A simple python script for the Raspberry Pi to blink an LED connected to BCM port 19. Connect an LED + to BCM port 19 and the LED - to GND - and don't forget to put a resistor in between (200 - 300 Ohm) - then run this container and watch your LED bl